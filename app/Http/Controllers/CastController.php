<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $casts = $users = DB::table('casts')->get();
  
        return view('casts.index', compact('casts'));
    }

    public function create()
    {
        return view('casts.create');
    }

    public function show($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();
        return view('casts.show', compact('cast'));
    }

    public function store(Request $request)
    {
        DB::table('casts')->insert([
            'name' => $request->name,
            'age' => $request->age,
            'bio' => $request->bio,
        ]);
        return redirect()->route('casts.index');
    }

    public function edit($id)
    {
        $cast = DB::table('casts')->where('id', $id)->first();;
        return view('casts.edit', compact('cast'));
    }

    public function update(Request $request, $id)
    {
        DB::table('casts')
            ->where('id', $id)
            ->update([
                'name' => $request->name,
                'age' => $request->age,
                'bio' => $request->bio
            ]);

        return redirect()->route('casts.index');
    }

    public function destroy($id)
    {
        DB::table('casts')->where('id', $id)->delete();
        return redirect()->route('casts.index');
    }
}
